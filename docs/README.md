# JAWS Documentation

## Repository for JAWS docs

This content was created by using sphinx and hosted by [readthedocs](https://sphinx-rtd-theme.readthedocs.io/en/stable/index.html).

### Install sphinx

```
apt-get install python3-sphinx ## Linux
brew install sphinx-doc ## macOS
```

### Create `conda env`

```
conda create --name sphinx
conda activate sphinx
pip install sphinx
pip install recommonmark
pip install -U sphinxcontrib-confluencebuilder
pip install sphinx_rtd_theme 
pip install sphinx-panels
```
  
### Sphinx Tutorials

- Follow sphinx tutorial in [readthedocs.org](https://docs.readthedocs.io/en/latest/intro/getting-started-with-sphinx.html).

Sphinx uses [reStructuredText](http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html).

### Create Initial Directory Structure

```
sphinx-quickstart
```

Now start adding content to the pages...


### Build `html` from source (`rst` or `md`) files

```
make html
```

### Quick View

```
open build/html/index.html
make clean
```

Once you are satisfied, add changes to this gitlab repo. 


## Register gitlab with readthedocs

https://docs.readthedocs.io/en/latest/intro/getting-started-with-sphinx.html

## View published docs

https://jaws-docs.readthedocs.io/en/latest/

## Publish BETA docs

When you want to publish a draft of the documentation you can create a Merge Request and push changes to the branch. Any pushes to the branch will create a pipeline that has a manual option to publish the documentation as beta which can be viewed at https://jaws-docs.readthedocs.io/en/beta.

# Publish pages to confluence

### Followed tutorial

https://sphinxcontrib-confluencebuilder.readthedocs.io/en/latest/tutorial.html


### Publish pages to confluence

```
on cori
# working dir
/global/cscratch1/sd/jfroula/

# create env
/global/dna/shared/data/jfroula/miniconda3/bin/virtualenv testenv
. ./testenv/bin/activate
cd testenv/

# install dependencies
pip install sphinxcontrib-confluencebuilder
pip install recommonmark

# clone repo

# add confluence stuff to conf.py

# this command should add pages to confluence (space JAWS)
make confluence
```
