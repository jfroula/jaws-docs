=============
Getting Help
=============

.. role::bash(code)

.. role:: darkorange


Create an Issue or Request for a new Feature
============================================

To report an issue or request support, please follow the appropriate procedure below based on your access and preferred platform:

.. tab-set::

    .. tab-item:: GitLab 

        #. Access your Repository: `jaws-support <https://code.jgi.doe.gov/advanced-analysis/jaws-support>`_.
        #. If you don’t have access, please request via Slack.
        #. Click on “New issue”.
        #. Please choose between Bugs and Features under Description. 
        #. IMPORTANT: Add `user_experience::user_request` Label to your issue. This is very important because it will notify us that the ticket was created, and also it will display on the Board. 

    .. tab-item:: JIRA

        To open a JIRA Ticket, go to `JGI Intranet -> SUPPORT -> JGI’s Analysis Workflow Service (JAWS) <https://intranet.lbl.gov/jgi/services/computers-networking/jaws/>`_.

    .. tab-item:: E-mail 

        GitLab and JIRA are only available for **JGI Staff**. 
        If you do not have access to these platforms, please send an email to: `jaws-support@lbl.gov <mailto:jaws-support@lbl.gov>`_.

Using SLACK to Ask a Question
=============================

For quick questions and discussions, join the **#jaws** channel on Slack at **joint-genome-inst.slack.com**.


See Current Issue Priorities & Vote
===================================

You can see what issues we are currently working on and what the priority ("Weight") is of each. You can also vote for issues that will move them up the priority list.

**To see the issue related to JAWS users:**

#. Click here to see the `Board <https://code.jgi.doe.gov/groups/advanced-analysis/-/boards/107>`_;

#. `Labels` are used to categorize the board. For example:

   * :darkorange:`user_experience::user_request` (first column), you can find all issues requested by Users;
   * :darkorange:`user_experience::jaws_request` (second column), you can find all the issues written by the JAWS team;

Example of the first two columns:

   .. figure:: /Figures/labels.png

**To vote:**

It is now possible to vote on the issues that you deem most important. 
Since we have many tickets open, we ask the Users to vote on only the three most important issues for you.

#. Go to the Board and click on the ticket.
#. On the right panel, you can add a +1 to the Weight. 
#. The new Weight is displayed on the Board, and we will consider moving the issue up the priority list, which is represented by the issue order. 
#. Since we have many new feature requests, we ask you to select only the three most important issues/features for you. It will allow us to prioritize the development of the features.


Request for Paired Programming
==============================

You can request to schedule a zoom meeting with a JAWS team member to work with you to get your workflow running via Cromwell and JAWS. 

Send an email to jaws-support@lbl.gov.

Via the email or during the meeting, please expect to answer the following:  

	1. Do you have either a process (set of well-defined manual steps) or a pipeline (steps already strung together by some script/framework)?   

	2. Do you have any documentation for your pipeline? Please provide the link.

	3. Do you have a good idea of the compute resource requirements for each step? (i.e. time, ram, cpu)   

	4. Were you the author of the process/pipeline?  Who else contributed to it and are they still available to answer questions if needed?   

	5. Do you have experience making Docker containers?   

	6. Do you have a hard deadline to get the WDL running?   

	7. Is the workflow to be publicly released?   

--------------------------


Contributors
============

Developers

  * Daniela Cassol 
  * Edward Kirton
  * Elais Player
  * Jeff Froula
  * Mario Melara
  * Seung-jin Sul
  * Stephan Trong
  * Ramani Kothadia

Documentation and WDL authoring

  * Jeff Froula
  * Daniela Cassol

User support

  * Jeff Froula
  * Daniela Cassol

System and integration testing

  * Jeff Froula
  * Daniela Cassol

Functional design consulting, project and resource coordination

  * Kjiersten Fagnan

