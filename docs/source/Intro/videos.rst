=============
Video Library
=============

by Lynn Langit
==============
* `Writing WDLs <https://www.youtube.com/playlist?list=PL4Q4HssKcxYv5syJKUKRrD8Fbd-_CnxTM>`_

by the JAWS Team
================
* `Introduction to creating docker images <https://youtu.be/Mc7DAAxjEV4>`_

* Setting up the environment for using JAWS commands: Coming soon!
