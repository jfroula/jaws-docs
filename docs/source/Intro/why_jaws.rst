====================
Why Use JAWS
====================

.. role:: bash(code)
   :language: bash


* With a simple command, :bash:`jaws submit`, you can submit a workflow. Running pipelines is made easier by conveniently masking the details of how and where the jobs run.

* You can monitor your jobs with simple commands, like :bash:`jaws log`. 

* You have access to multiple compute clusters operated by different national labs. For example, to submit to the compute site at Pacific Northwest National Lab (PNNL), the command would be :bash:`jaws submit <wdl> <inputs.json> tahoma`, where "tahoma" is the name of the cluster.
 
* Your workflows are easily scalable. For example, if you have tasks that can be run in parallel, you can select multiple nodes.
 
.. figure:: /Figures/why_jaws.svg
   :class: with-shadow
   :scale: 50%

   Here are some additional benefits to using JAWS. 


Leverages Community Supported Tools
===================================

JAWS is built upon a foundation of robust, community-supported tools, ensuring reliability and widespread compatibility.

.. figure:: /Figures/technologies_used.png
   :align: center
   :scale: 80%

----------------

Technologies used:
++++++++++++++++++

- **Cromwell**: Executes workflows written in Workflow Description Language (WDL). For more details, see the `WDL Specification <https://github.com/openwdl/wdl/blob/main/versions/1.0/SPEC.md>`_.
- **Shifter, Singularity**: These container platforms define the runtime environment for tasks.
- **HTCondor**: Distributes jobs across multiple compute clusters, such as "`dori`" and "`jgi`."
- **Globus**: Facilitates file transfers between various endpoints using GridFTP.
- **REST APIs**: Enable communication between different JAWS components through RESTful interfaces.
- **RabbitMQ**: Acts as a message broker, managing the communication of workflow tasks between Cromwell and the compute cluster workers.

