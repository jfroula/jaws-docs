=============
JAWS Commands
=============

.. role:: bash(code)
   :language: bash

.. warning::
   Before these commands will work, you need to set up everything:
   See `Quickstart Example <jaws_quickstart.html#begin-setup.html>`_ for full setup instructions.

Example submitting a workflow:

:bash:`jaws submit <WDL> [inputs json] <SITE>`

Usage: jaws [OPTIONS] COMMAND [ARGS]...
---------------------------------------
Run a command with :bash:`--help` to see it's options.

.. code-block:: text

    jaws --help 
    
    Usage: jaws [OPTIONS] COMMAND [ARGS]...
    JGI Analysis Workflows Service

    Options:
      --config TEXT     JAWS config file
      --user TEXT       User config file
      --log TEXT        Log file
      --log-level TEXT  Logging level [debug|info|warning|error|critical]
      -h, --help        Show this message and exit.

    Commands:
    * admin          Administrator Commands.
      cancel         Cancel a run; prints whether aborting was successful or not.
      cancel-all     Cancel all active runs.
      download       Force the download of a failed/cancelled run.
    * get            The `get` command has been deprecated; this will just `mv`...
      get-user       Get current user's settings.
      health         Current system status.
    * history        Print a list of the user's past runs.
      info           JAWS version and info.
      inputs         Generate inputs template (JSON) from workflow (WDL) file.
      list-sites     List available compute Sites.
    * log            View the log of Run state transitions for the workflow as...
    * purge          Remove cromwell-execution directory for a specific run
    * queue          List of user's current runs.
      resubmit       Resubmit a run (at same compute-site).
    * status         Print the current status of a run.
    * submit         Submit a run for execution at a JAWS-Site.
      task-log       The `task-log` command has been deprecated.
      task-summary   The `task-summary` command has been deprecated.
    * tasks          Get Task info.
    * teams          Teams Commands.
    * update-user    Update existing user in JAWS.
      validate       Validate a WDL using miniwdl.


The commands with an asterik have additional options. Expand the below commands to see them.

.. dropdown:: admin
    :color: info
    :animate: fade-in
    
    .. code-block:: text
    
        Usage: jaws admin [OPTIONS] COMMAND [ARGS]...

          Administrator Commands

        Options:
          -h, --help  Show this message and exit.

        Commands:
          add-team       Create a new user team
          chown-team     Change the team's owner.
          create-user    Create new user and get JAWS OAuth access token.
          del-team       Delete a user team
          disable-sites  Turn off site(s) ex) jaws disable-sites Dori jaws...
          enable-sites   Turn on site(s) ex) jaws enable-sites Dori jaws...
          queue-wait     List compute Sites' estimated queue-wait times (in...


.. dropdown:: get
    :color: info
    :animate: fade-in
    
    .. code-block:: text
    
        Usage: jaws get [OPTIONS] RUN_ID DEST
        
          The `get` command has been deprecated; this will just `mv` your output folder

        Options:
          --complete  This does nothing; maintained for backwards compatibility.
          --flatten   This does nothing; maintained for backwards compatibility.
          --quiet     This does nothing; maintained for backwards compatibility.
          -h, --help  Show this message and exit.


.. dropdown:: history
    :color: info
    :animate: fade-in
    
    .. code-block:: text

        Usage: jaws history [OPTIONS]
        
          Print a list of the user's past runs.
        
        Options:
          --days INTEGER    history going back this many days; default=1
          --site TEXT       limit results to this compute-site; default=all
          --result TEXT     limit results to this result; default=any
          --tag TEXT        List runs from single tag; default=False
          --wdl TEXT        Filter runs by wdl file name; default=any
          --json-file TEXT  Filter runs by json file name; default=any
          --user TEXT       Filter runs by user; default=any
          --verbose         Return more fields; default=False
          --all             List runs from all users; default=False
          -h, --help        Show this message and exit.


.. dropdown:: log
    :color: info
    :animate: fade-in
    
    .. code-block:: text     
    
        Usage: jaws log [OPTIONS] RUN_ID
        
          View the log of Run state transitions for the workflow as a whole.
        
        Options:
          --fmt TEXT  the desired output format: [text|json|tab].
          -h, --help  Show this message and exit.


.. dropdown:: purge
    :color: info
    :animate: fade-in
    
    .. code-block:: text   

        Usage: jaws purge [OPTIONS] RUN_ID

          remove cromwell-execution directory for a specific run

        Options:
          -h, --help  Show this message and exit.



.. dropdown:: queue
    :color: info
    :animate: fade-in
    
    .. code-block:: text   

        Usage: jaws queue [OPTIONS]
        
          List of user's current runs
        
        Options:
          --site TEXT         limit results to this compute-site; default=all
          --all-users, --all  List runs from all users; default=False
          --fmt TEXT          the desired output format: [text|json|tab]
          -h, --help          Show this message and exit.


.. dropdown:: status
    :color: info
    :animate: fade-in
    
    .. code-block:: text   

        Usage: jaws status [OPTIONS] RUN_ID
        
            Print the current status of a run.

        Options:
          --verbose   This does nothing; maintained for backwards compatibility.
          -h, --help  Show this message and exit.

.. dropdown:: submit
    :color: info
    :animate: fade-in
    
    .. code-block:: text   

        Usage: jaws submit [OPTIONS] WDL_FILE [INPUTS]... SITE
        
          Submit a run for execution.
        
        Options:
          --tag TEXT          identifier for the run
          --no-cache          Disable call-caching for this run
          --quiet             Don't print copy progress bar
          --sub TEXT          Subworkflows zip (optional; by default, auto-generate)
          --team TEXT         User-team to associate with this run
          --webhook TEXT      If provided, JAWS will POST to this URL when Run completes.
          --forcequeue        Submit Run to an unavailable site.
          --overwrite-inputs  Overwrite input files that already exist in JAWS cache.
          -h, --help          Show this message and exit.


.. dropdown:: tasks
    :color: info
    :animate: fade-in
    
    .. code-block:: text 

        Usage: jaws tasks [OPTIONS] RUN_ID
        
          Get Task logs and current status
        
        Options:
          --fmt TEXT  the desired output format: [txt|json|tab].
          -h, --help  Show this message and exit.


.. dropdown:: teams
    :color: info
    :animate: fade-in
    
    .. code-block:: text 

        Usage: jaws teams [OPTIONS] COMMAND [ARGS]...

          Teams Commands
        
        Options:
          -h, --help  Show this message and exit.

        Commands:
          add-site       Grant team access to a site (restricted)
          add-user       Add a user to a team
          get-site-path  Get a team's site config
          list           List all teams
          list-sites     List a team's sites.
          members        List the users associated with a team.
          my-teams       List the teams to which you belong
          remove-site    Remove team access to a site (restricted)
          remove-user    Remove a user from a team
          set-site-path  Configure a team's folder at a jaws-site (restricted)


.. dropdown:: update-user
    :color: info
    :animate: fade-in
    
    .. code-block:: text 

        Usage: jaws update-user [OPTIONS]

          Update existing user in JAWS

        Options:
          --email TEXT          Your email address
          --name TEXT           Your full name
          --slack_id TEXT       Your slack Member ID
          --slack_webhook TEXT  Your slack webhook
          -h, --help            Show this message and exit.



Examples
--------

**See status of the JAWS services**
+++++++++++++++++++++++++++++++++++

Not all sites are represented here yet.

.. code-block:: text

    jaws health
    {
        "Central": "UP",
        "RabbitMQ": "UP",
        "defiant-Site": "UP",
        "dori-Site": "UP",
        "jgi-Site": "UP",
        "nmdc-Site": "UP",
        "nmdc_tahoma-Site": "UP",
        "perlmutter-Site": "UP",
        "tahoma-Site": "UP"
    }


**To run a wdl**
++++++++++++++++

To run a workflow, you will need two files: a WDL file (`.wdl`) and an inputs (`.json`) file. 
For the latter, you can generate one from scratch by running :bash:`jaws inputs <WDL>`, which will print out a template based on the WDL, you just need to fill out the values.

A simple example template would look like:

.. code-block:: text

    {
      fq_count.fastq_file: File
    }

**To submit a run**
+++++++++++++++++++

.. code-block:: text

  jaws submit my.wdl my.json dori

  # output looks like
  {
    run_id: 7235,
  }

Include a tag for your run to help keep track of things. :bash:`jaws status` will display the tag.

.. code-block:: text

  jaws submit --tag 'some useful info' my.wdl my.json dori

Run with Cromwell's call-caching off. 
Call-caching will allow you to re-run JAWS submissions without re-running tasks that completed successfully. 
However, this is not always desirable and you can turn caching off, as follows:

.. code-block:: text

  jaws submit --no-cache my.wdl my.json dori


**Monitor your Run**
++++++++++++++++++++

The examples are in order of verbosity.

.. code-block:: text

  # overview of the whole WDL
  jaws status --brief 7235

  or

  # the times each stage started, e.g. queued, running, etc.
  jaws log 7235

  or

  # the times each task entered each stage 
  jaws tasks 7235


**Understanding the Stages**
++++++++++++++++++++++++++++

These are the possible states, in order, that a JAWS run passes through. 

.. code-block:: text

    created:             The Run was accepted and a run_id assigned.
    upload queued:       The Run's input files are waiting to be transferred to the compute-site.
    uploading:           Your Run's input files are being transferred to the compute-site.
    upload failed:       The transfer of your run to the compute-site failed.
    upload inactive:     Globus transfer stalled.
    upload complete:     Your Run's input files have been transferred to the compute-site successfully.
    ready:               The Run has been transferred to the compute-site.
    submitted:           The run has been submitted to Cromwell and tasks should start to queue within moments.
    submission failed:   The run was submitted to Cromwell but rejected due to invalid input.
    queued:              At least one task has requested resources but no tasks have started running yet.
    running:             The run is being executed; you can check `tasks` for more detail.
    succeeded:           The run has completed successfully.
    failed:              The run has failed and will not be transferred unless you run the 'download' command.
    complete:            Supplementary files have been written to the run's output dir.
    finished:            The task-summary has been published to the performance metrics service.
    cancelling:          Your run is in the process of being canceled.
    cancelled:           The run was cancelled by either the user or an admin.
    download queued:     Your Run's output files are waiting to be transferred from the compute-site.
    downloading:         The Run's output files are being transferred from the compute-site.
    download failed:     The Run's output files could not be transferred from the compute-site.
    download inactive:   Globus transfer stalled.
    download complete:   Your Run's output (succeeded or failed) have been transferred to the team outdir.
    download skipped:    The run was not successful so the results were not downloaded.
    fix perms queued:    The fix permissions request has been queued.
    fix perms complete:  The permissions of the outputs have been changed.
    fix perms failed:    The permissions of the outputs could not be changed.
    sync complete:       Inode metadata sync delay complete.
    slack succeeded:     Notification of run completion was sent via slack.
    slack failed:        Notification of run completion could not be sent via slack.
    post succeeded:      Notification of run completion was POSTed to webhook URL.
    post failed:         Notification of run completion could not be POSTed to webhook URL.
    done:                The run is complete.


**Get current or old history of jobs owned by you**
+++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: text

   # get list of your currently running jobs
   jaws queue
   
   # view history of your jobs for last 7 days on site Dori
   jaws history --days=7 --site=dori

**Debugging**
+++++++++++++

The :bash:`errors.json` file is a catch-all command for viewing errors. 
It is created by JAWS after a run has completed and it will be transfer to the teams output directory.

The :bash:`errors.json` file should be capturing errors from:

  1. cromwell 
  2. the WDL tasks
  3. HTCondor backend
  4. Slurm

Sometimes there is additional information in the files created by cromwell: 

.. code-block:: text

    script.submit - contains the commands used by cromwell to run the script file.
    script        - the commands representing cromwell boilerplate stuff in addition to your commands from the wdl-task.  
    stderr        - the stderr from running script.
    stdout        - the stdout from running script.
    stderr.submit - the stderr from submission script.
    stdout.submit - the stdout from submission script.


These files will be transferred to the team's output directory solely for tasks that have failed, for the purpose of debugging.

**Getting your output**
+++++++++++++++++++++++

The final outputs from the workflow will be moved to your team's directory. If a run fails, the resulting final outputs will still be transferred. 
Furthermore, for any tasks that fail, the complete Cromwell execution folder for that task will be copied over.

:bash:`jaws status <RUN_ID>` display the :bash:`output_dir` for a specific run.

**Specialty Commands**
++++++++++++++++++++++

This command uses the `miniwdl <https://miniwdl.readthedocs.io/en/latest/index.html>`_ as a linter for your WDLs. 
You would use this when developing a WDL.

.. code-block:: text

    jaws validate my.wdl


The :bash:`--user` flag allows someone to use a different :bash:`jaws token`` than the default. 
This way, you can have a token representing a user like 'rqc' or 'jaws-admin' with certain permissions.
Then multiple people from a group can use this token to have access to certain files.

.. code-block:: text

    jaws --user <~/jaws.conf> <command>



JAWS Deprecated Commands
------------------------

In this section, you'll find a compilation of commands deprecated in the previous releases.
For each command, we have also provided guidance on finding its current alternative. 
If you want more details about each release, please check the Release notes.

- :bash:`jaws task-log` and :bash:`jaws task-summary`
    - These two commands have been merged and into a new command, :bash:`jaws tasks <RUN_ID>`.
    - For backward compatibility, :bash:`jaws task-log` will be reporting the output of :bash:`jaws tasks`.

- :bash:`jaws get`

  - As we transition to copying the output files to the JAWS Teams directory, we are deprecating out the :bash:`jaws get` command. To ensure backward compatibility, this command will remain functional for a few more cycles, serving solely to copy the final workflow outputs. 

  - :bash:`jaws status <ID>` will provide the path to final path for the output files. Please check :bash:`output_dir`. Then, you can find the expected file tree structure:

    .. code-block:: text

        /<TEAM PATH>/<RUN_ID>/<Cromwell_ID>

- :bash:`jaws status –verbose` changed
    - :bash:`jaws status` is now verbose by default. If you prefer the short version, please use :bash:`jaws status --brief`. 
    - To ensure backward compatibility, when you use the command :bash:`jaws status --verbose`, it will issue a warning and additionally display the output of :bash:`jaws status`.
 
- :bash:`jaws outfiles`, :bash:`jaws outputs`, :bash:`jaws metadata` and :bash:`jaws errors`

  - All these commands were deprecated as part of the effort to refactor :bash:`jaws metadata` command (that was using cromwell metadata). As users are submitting large workflows (> 10k tasks), cromwell metadata became too expensive to query and would sometimes timeout. Instead, we now wait for the run to finish and then write some associated reports (e.g. errors, outfiles, and outputs) to disc. These `json` files are written to the run’s execution directory and transfer to the Team's directory.
  - How to find the report files:

    .. code-block:: text
      
      ls /<TEAM PATH>/<RUN_ID>/<Cromwell_ID>
      
      <workflow>.wdl 
      <inputs>.json 
      errors.json 
      metadata.json 
      output_manifest.json 
      outputs.json 
      summary.json  
      tasks.json
