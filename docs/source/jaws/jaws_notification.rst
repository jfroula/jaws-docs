==================
JAWS Notifications
==================

.. role:: bash(code)
   :language: bash


Slack Notifications
===================

JAWS has transitioned from email to Slack notifications to provide real-time updates on the status of runs. This section covers how to set up Slack notifications for your JAWS account.

How to set up Slack Notification?
---------------------------------

**Obtain a Slack Webhook URL**

To receive Slack notifications, you need to set up a :bash:`webhook` in Slack. 
Detailed instructions for obtaining your Slack webhook URL can be found `here <https://docs.google.com/presentation/d/1FAGz1DFPaB-wF-aX9ni4LlTAuVMhYDnjA49IM_tYons/edit?usp=sharing>`_.

**Update Your JAWS Account with Slack Details**

Once you have your Slack webhook URL, update your JAWS user profile with the Slack details. 
 Use the following command, replacing `<Member_ID>` and `<WebHook_URL>` with your actual Slack member ID and webhook URL:


.. code-block:: text

  jaws update-user --slack_id <Member_ID> --slack_webhook <WebHook_URL>


Retrieving User Settings
------------------------

**Check Current User Settings**

To verify the current settings of your user account, including configured Slack details, 
use the :bash:`jaws get-user` command. 
This command is particularly useful for confirming that your Slack notifications are set up correctly.

.. code-block:: text

  jaws get-user
  {
      "email": "dcassol@lbl.gov",
      "name": "Daniela Cassol",
      "slack_id": "<Member_ID>",
      "teams": [
          "dsi-aa",
          "nmdc"
      ],
    "uid": "dcassol"
  }