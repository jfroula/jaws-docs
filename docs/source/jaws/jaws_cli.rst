=================================================
Configuring the JAWS Client: A Step-by-Step Guide
=================================================

.. role:: bash(code)
  :language: bash


Summary
=======

The JAWS Client is a command-line interface (CLI) that allows you to interact with the JAWS service. 
This guide will help you set up the JAWS Client on one or more locations where the JAWS Client is installed.

Prerequisites
=============

- **JAWS Token**: You must have a JAWS token to access the JAWS Client.
- **Location of your input data**:
    - **DORI**: If your data is stored on Dori, you must have a Dori account.
    - **NERSC**: If your data is stored on the NERSC file system, you must have a NERSC account and be a member of the `genome` group (or `nmdc` for the NMDC project, or `kbase` for the KBASE project).

If you have questions about how to get an account, please get in `touch with our team <../Intro/contact_us.html>`_.

Steps to Set Up JAWS Client
===========================

These are the steps you'll be required to complete to start running a pipeline in JAWS:

    #. Obtain a JAWS Token;
    #. Create a configuration file to store your token;
    #. choose how you'd like to access the JAWS Client:
        * You have three options for accessing the JAWS Client:
            * `Option 1 <jaws_cli.html#module-load-jaws>`_: activate the JAWS virtual environment using `module load jaws`;
            * `Option 2 <jaws_cli.html#jaws-client-container>`_: use `jaws client` container;
            * `Option 3 <jaws_cli.html#install-jaws-client-package-into-your-virtual-environment>`_: install the `jaws-client` into your own virtual environment using `pip`.

Let's get started!

JAWS TOKEN
==========

.. admonition:: JAWS TOKEN

    Before completing the JAWS setup below, you need to get a **JAWS token** from the JAWS admin. 
    
    **Please fill this out** `form <https://docs.google.com/forms/d/e/1FAIpQLSds8rAJfcRAp8nRoKCf7sv7XO4eb4Rn3XYkQ8EPDiyCR3bh6w/viewform?usp=sf_link>`_.
    The JAWS Team will contact you soon!

Setting up JAWS Client
======================

1. Create a config file that has the JAWS token in it:
------------------------------------------------------
Select the cluster where your data is stored and to which you will be submitting to JAWS.
Next, copy the :bash:`jaws.conf` file to your home directory (:bash:`$HOME`).
Ensure that the file permissions are set so that only you can access it.

.. tab-set::

    .. tab-item:: DORI
        :sync: key1

        .. code-block:: text

            cp /clusterfs/jgi/groups/dsi/homes/svc-jaws/dori-prod/jaws.conf ~/jaws.conf
            chmod 600 ~/jaws.conf

    .. tab-item:: NERSC - Perlmutter
        :sync: key2

        .. code-block:: text

            cp /global/cfs/cdirs/jaws/perlmutter-prod/jaws.conf ~/jaws.conf
            chmod 600 ~/jaws.conf

    .. tab-item:: NERSC - NMDC
        :sync: key3

        .. code-block:: text

            cp /global/cfs/cdirs/m3408/jaws-install/jaws-client/nmdc-prod/jaws.conf ~/jaws.conf
            chmod 600 ~/jaws.conf

    .. tab-item:: NERSC - KBASE
        :sync: key4

        .. code-block:: text

            cp /global/cfs/cdirs/kbase/jaws/jaws-client/kbase-prod/jaws.conf ~/jaws.conf
            chmod 600 ~/jaws.conf

    .. tab-item:: TAHOMA - NMDC
        :sync: key5

        .. code-block:: text

            cp /tahoma/mscnmdc/jaws-install/jaws-client/nmdc_tahoma-prod/jaws.conf ~/jaws.conf
            chmod 600 ~/jaws.conf

2. Add your :bash:`token` and :bash:`default_team` to :bash:`~/jaws.conf`:
--------------------------------------------------------------------------

.. code-block:: text

      [USER]
      token = <your token>
      default_team = <team ID>


Find `here <jaws_teams.html>`_ information about JAWS Teams.


Use JAWS CLI
=============

To use the JAWS CLI, you can choose from three options tailored to different needs and setups. 

1. You could activate the JAWS environment with the command :bash:`module load jaws` for easy integration. 

2. Alternatively, using the :bash:`jaws client` container offers a more isolated setup. 

3. If you prefer direct integration with your existing Python projects, installing :bash:`jaws-client` package into your virtual environment is also an option.

1. :bash:`module load jaws`
---------------------------

You can use `Environment Modules <https://modules.readthedocs.io/en/latest/>`_ to load JAWS in the environment.

General Usage:

.. code-block:: text
        
    module use <PATH/jaws/modulefiles/>
    module load jaws
    
.. note::  
	The purpose of :bash:`module use` is to point to our custom module files.
	You can add one or both of these lines to your :bash:`~/.bashrc` file.

.. tab-set::

    .. tab-item:: DORI
        :sync: key1

        .. code-block:: text

            module use /clusterfs/jgi/groups/dsi/homes/svc-jaws/modulefiles
            module load jaws

    .. tab-item:: NERSC - Perlmutter
        :sync: key2

        .. code-block:: text

            module use /global/cfs/cdirs/jaws/modulefiles
            module load jaws

    .. tab-item:: NERSC - NMDC
        :sync: key3

        If you are part of `NMDC` project, please use:

        .. code-block:: text

            module use /global/cfs/cdirs/m3408/jaws-install/jaws-client/modulefiles
            module load jaws

    .. tab-item:: NERSC - KBASE
        :sync: key4

         If you are part of `KBase` project on NERSC, please follow:

        .. code-block:: text

            module use /global/cfs/cdirs/kbase/jaws/modulefiles
            module load jaws

    .. tab-item:: TAHOMA - NMDC
        :sync: key5

         If you are part of `NMDC` project on Tahoma, please follow:

        .. code-block:: text

            module use /tahoma/mscnmdc/jaws-install/jaws-client/modulefiles
            module load jaws            


* To **deactivate** the environment, use the following:

    .. code-block:: text

        module unload jaws

.. dropdown:: Useful commands :ref:`🔗 <Useful commands>`
    :color: light
    :animate: fade-in
    :name: Useful commands

    .. code-block:: text

        module list        ## List actyive modules in the environment
        module avail jaws  ## List available JAWS modules

2. JAWS Client Container
------------------------

You can use the JAWS Client Container.

.. tab-set::

    .. tab-item:: DORI
        :sync: key1

        .. code-block:: text
        
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/clusterfs/jgi/groups/dsi/homes/svc-jaws/dori-prod/jaws-prod.conf apptainer run docker://doejgi/jaws-client:latest jaws --help

        - Append to the end of your :bash:`~/.bashrc` on DORI:

        .. code-block:: text
        
            jaws() {
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/clusterfs/jgi/groups/dsi/homes/svc-jaws/dori-prod/jaws-prod.conf apptainer run docker://doejgi/jaws-client:latest jaws "$@"
            }

    .. tab-item:: NERSC - Perlmutter
        :sync: key2

        .. code-block:: text
        
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/global/cfs/cdirs/jaws/perlmutter-prod/jaws-prod.conf shifter --image=doejgi/jaws-client:latest jaws --help

        - Append to the end of your :bash:`~/.bashrc`:
        
        .. code-block:: text
        
            jaws() {
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/global/cfs/cdirs/jaws/perlmutter-prod/jaws-prod.conf shifter --image=doejgi/jaws-client:latest jaws "$@"
            }

    .. tab-item:: NERSC - NMDC
        :sync: key3

        If you are part of `NMDC` project, please use:

        .. code-block:: text
        
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/global/cfs/cdirs/m3408/jaws-install/jaws-client/nmdc-prod/jaws-prod.conf shifter --image=doejgi/jaws-client:latest jaws --help

        - Append to the end of your :bash:`~/.bashrc`:
        
        .. code-block:: text
        
            jaws() {
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/global/cfs/cdirs/m3408/jaws-install/jaws-client/nmdc-prod/jaws-prod.conf shifter --image=doejgi/jaws-client:latest jaws "$@"
            }

    .. tab-item:: NERSC - KBASE
        :sync: key4

        If you are part of `KBase` project, please use:

        .. code-block:: text
        
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/global/cfs/cdirs/kbase/jaws/jaws-client/kbase-prod/jaws-prod.conf shifter --image=doejgi/jaws-client:latest jaws --help

        - Append to the end of your :bash:`~/.bashrc`:
        
        .. code-block:: text
        
            jaws() {
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/global/cfs/cdirs/kbase/jaws/jaws-client/kbase-prod/jaws-prod.conf shifter --image=doejgi/jaws-client:latest jaws "$@"
            }

    .. tab-item:: TAHOMA - NMDC
        :sync: key5

         If you are part of `NMDC` project on Tahoma, please follow:

        .. code-block:: text
        
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/tahoma/mscnmdc/jaws-install/jaws-client/nmdc_tahoma-prod/jaws-prod.conf apptainer run docker://doejgi/jaws-client:latest jaws --help

        - Append to the end of your :bash:`~/.bashrc`:

        .. code-block:: text
        
            jaws() {
            JAWS_USER_CONFIG=~/jaws.conf JAWS_CLIENT_CONFIG=/tahoma/mscnmdc/jaws-install/jaws-client/nmdc_tahoma-prod/jaws-prod.conf  apptainer run docker://doejgi/jaws-client:latest jaws "$@"
            }          


3. Install `jaws-client` Package into Your Virtual Environment
--------------------------------------------------------------

- Back on your terminal (i.e. on Dori):

    - create a `venv` and activate it (or active your existing `venv`):

    .. code-block:: text

        python3 -m venv <some-env-name>
        source <some-env-name>/bin/activate

    - Install `jaws-client` package:

    .. code-block:: text

        pip install jaws-client==2.5.2 --index-url https://code.jgi.doe.gov/api/v4/projects/312/packages/pypi/simple 

Note: jaws-client requires Python version between 3.10 and 3.12.

    - Run the JAWS Client Python Library:

       We have a dedicated session on how to use the JAWS Client Python Library. 
       Please refer to `JAWS API <jaws_api.html>`_ for more information.
