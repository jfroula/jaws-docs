=============
JAWS Policies
=============

.. role:: listsize
.. role:: textborder
.. role:: bash(code)

JAWS Fair-Share Policies
========================

We are committed to ensuring equitable access to JAWS resources for all users, which is why we have implemented JAWS Execution Throttling that facilitates our fair-share policies.

These policies are designed to manage resource allocation on multiple layers, ensuring that no single user can overload the system. 
Below are the specifics of our two-layer control system:

Run-Level Control
-----------------

  - *Limit*: A maximum of 10 runs can be executed concurrently per site per user.
  
    - NOTE: We have increased this limit to 30 runs per user on DORI. 
  - *Queue*: Any additional run submissions beyond this limit will be automatically placed in the JAWS Queue.
  - *Processing*: These queued runs will be initiated as soon as one of the currently active runs is completed.

Task-Level Control
------------------

  - *Limit*: A cap of 300 concurrent *tasks* is set per workflow.
  - *Queue*: Any tasks submitted beyond this 300-task limit will be placed in a queue.
  - *Processing*: These queued tasks will be executed as soon as one of the active tasks is completed.


JAWS Purge Policies
====================

To manage storage effectively, input files (located in the JAWS Staging Area) and Cromwell execution directories (`workflow_root`) are automatically removed after 10 days.

However, don’t worry—if your run was successful, JAWS will have already transferred the output data to the JAWS Team directory.

For more detailed information on how to find the output data, please refer to `JAWS Teams <jaws_teams.html>`_.

