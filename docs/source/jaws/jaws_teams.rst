==========
JAWS TEAMS
==========

.. role:: bash(code)
   :language: bash

"JAWS Teams" is a feature designed to simplify user management and enhance the efficiency 
of output file sharing and delivery within your teams.

Commands
========

- **List All Available Teams**

.. code-block:: text

  jaws teams list
    [
        "gt-ga",
        "nmdc",
        "dsi-ii",
        "bionano",
        "sc-mcr",
        "dsi-syseng",
        "ornl",
        "workshop",
        "plant_ha",
        "kbase",
        "mycocosm",
        "plantcompgen",
        "gaa",
        "gt-seqtech",
        "funcgen",
        "sc-mds",
        "gt-usa",
        "micro-scale",
        "rqc",
        "dsi-aa",
        "gt-syn",
        "phytzm"
    ]


- **List Teams You Belong To**

.. code-block:: text

  jaws teams my-teams
  [
      "dsi-aa"
  ]

- **List Team Members**

.. code-block:: text

  jaws teams members dsi-aa
  [
      "dcassol",
  ...
  ]

- **Get a Team's Designated Output Path**

.. code-block:: text

  jaws teams get-site-path dsi-aa dori
  "/clusterfs/jgi/scratch/dsi/aa/jaws/dsi-aa/output"

Manage Team Settings and Memberships
====================================

**Responsibilities of the Team's Owner**

Team's owner has power and responsibilities, including managing the team's output path for each site, adding and deleting users from the team.

**Commands for Team Management**

The following commands allow the team owner to perform these tasks:

.. code-block:: text

  # Set the site-specific output path for the team
  jaws teams set-site-path <TEAM_ID> <SITE_ID> <PATH>

  # Add a new user to the team
  jaws teams add-user <TEAM_ID> <USER_ID>

  # Remove a user from the team
  jaws teams remove-user <TEAM_ID> <USER_ID>

**Setting Correct Permissions**

When setting a new path, the team's owner must set the correct permissions to 
ensure access for JAWS to read and write to the new location.

.. tab-set::
    .. tab-item:: NERSC - Perlmutter

       For paths located on NERSC, set the permissions using the following command:

        .. code-block:: text

            setfacl -m d:g:"genome":rwx "<PATH>"

    .. tab-item:: NERSC - NMDC

        For paths located on NERSC for the NMDC project, set the permissions using the following command:

        .. code-block:: text

            setfacl -m d:g:"nmdc":rwx "<PATH>"

    .. tab-item:: NERSC - KBase

        For paths located on NERSC for the KBase project, set the permissions using the following command:

        .. code-block:: text

            setfacl -m d:g:"kbase":rwx "<PATH>"


How to use Jaws Teams?
======================

When submitting a new run, you can use :bash:`--team` flag. The outputs for this run will be transferred to the team's path. 

.. code-block:: text

  jaws submit align_final.wdl inputs.json dori --team=dsi-aa

As an easy alternative, you can set the :bash:`default_team` at your :bash:`jaws.conf` file:

.. code-block:: text

  vi ~/jaws.conf

  [USER]
  token = <TOKEN>
  default_team = dsi-aa

Important: Do not use quotes for the team's name.

The Jaws Team's feature will transfer the final workflow outputs from `cromwell-execution` to the path defined for your team. 

How do I find the output data?
==============================

:bash:`jaws status <RUN_ID>` will provide the path to final path. Please check :bash:`output_dir`.

Then, you can find the expected file tree structure:

.. code-block:: text

  /<TEAM PATH>/<USER_ID>/<RUN_ID>/<Cromwell_ID>

JAWS will copy the following in case of a successful run:

- Workflow outputs;
- Supplementary files:
 
    - :bash:`*.wdl`;
    - :bash:`*.json`;
    - errors.json;
    - metadata.json;
    - output_manifest.json;
    - outputs.json;
    - summary.json;
    - tasks.json


In case of a **FAILED** run, JAWS will skip the transfer of the output files. 
However, it's possible to 'force' the download, by using :bash:`jaws download <RUN_ID>`. 
In this case, JAWS will copy the following:

- Workflow outputs;
- Supplementary files;
- Failed tasks' cromwell-execution folder only.

As we transition to copying the output files, we are deprecating out the :bash:`jaws get` command. 
To ensure backward compatibility, this command will remain functional for a few more months, serving solely to copy the final workflow outputs. 
However, please note that we plan to discontinue this command entirely in upcoming cycles.

