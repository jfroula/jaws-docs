====================
Upcoming JAWS Events
====================

.. role:: bash(code)
   :language: bash

JGI Workshop Series
===================

JGI is hosting a series of workshops to help users get started with JAWS.
Please see the schedule for upcoming workshops and the material of previous workshops at `training.jgi.doe.gov <http://training.jgi.lbl.gov/>`_.

JAWS Calendar
==============

.. raw:: html

    <iframe src="https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=America%2FLos_Angeles&src=Y18xY2NyMG5vM3JmcWp1djNkc2U3a2o2Z2R2b0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&color=%23039BE5" style="border:solid 1px #777" width="700" height="600" frameborder="0" scrolling="no"></iframe>


.. hint::
   
   To sign up for JAWS Calendar, click the '**+Google Calendar**' button on the bottom right.
