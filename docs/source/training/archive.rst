=================
Workshops Archive
=================

.. role:: bash(code)
   :language: bash

JAWS Hackathon - January 09 and 10, 2023
===================================================

JAWS (JGI Analysis Workflow Service) Team is hosting the second event: **JAWS Hackathon**. 
The **hybrid event** will take place on January 09 and 10 (9:30AM to 4:30PM) at JGI.  

We will focus on building WDLs (Workflow Description Language) and docker images that will be required for JGI production pipelines. We will be working from this list as it will be a continuation of efforts from last time. Tasks will be contributed to the shared WDL Tasks Repo.

The attendees will be divided into smaller groups, and beginners will be paired with experienced WDL developers. 

Help us build a diverse community of people who want to innovate, explore, and implement more efficient, robust, and reproducible workflows. 

Interested? 

.. button-link:: https://docs.google.com/forms/d/e/1FAIpQLSfqMPAYE8EbyFQD2ymv5juG7_8rn3WYyWD9dg_Bs678neERAw/viewform?usp=sf_link
    :color: primary
    :shadow:

    Register here!

We are happy to answer any questions you may have at jaws-support@lbl.gov or at the #jaws slack channel.


JAWS Halloween Hackathon - Oct 31st and Nov 1st, 2022
=====================================================

The first JAWS Halloween Hackathon will take place on **Oct 31st and Nov 1st, 2022**, at the JGI. This will be a **hybrid event** hosted by the JAWS (JGI Analysis Workflow Service) team. 

We will focus on building WDLs and docker images that will be required for JGI production pipelines. In addition to giving you some hands-on experience, you will be contributing to the JGI subworkflow library.


Participants must have previous experience developing WDLs or have attended our Intro to WDL workshop. The attendees will be divided into smaller groups, and beginners will be paired with experienced WDL developers. 

On October 31st, from 12 to 2 PM, we would like to connect the pipelines with the people that are interested in them in teams of 2-4 people.  

The next day, November 1st, from 12 to 4 PM, we will start the development of the WDLs. There will be JGI & JAWS mugs and hoodies for certain benchmarks met, and pizza will be served at 12 PM both days.


Help us build a diverse community of people who want to innovate, explore, and implement more efficient, robust, and reproducible workflows. More details about the hackathon and the prizes for the competition will be announced soon. 


We are planning a Halloween Happy Hour, so we encourage you to participate in person. 

We are happy to answer any questions you may have at jaws-support@lbl.gov or at the #jaws-hallow-thon slack channel.


JAWS Workshop - 08/15, 08/18, 08/22, 09/12, and 09/19
=====================================================

JAWS (JGI Analysis Workflow Service) is hosting our workshop sessions on JAWS implementation, developing WDL (Workflow Description Language) workflows, Docker containers, and Condor. These are meant to be self-contained modules that will be offered regularly, so you can choose to attend any one, any time, and in any order.

- **Intro to WDL - 08/15/2022**

We will cover the basics of creating your own WDL with hands-on activities. Attendees will run through tutorials to showcase common WDL-development scenarios.

.. button-link:: https://docs.google.com/presentation/d/12zq846Bvt5cTfQXsCkpBn8tj-JFFvoSN9uoedkQ0RcA/edit?usp=sharing
    :color: primary
    :shadow:

    Slides

- **Intro to Docker Containers - 08/18/2022**

We will introduce Docker containers by demonstrating how to create Dockerfiles, build images and understand layers and tags, and how to push images to a registry. 

.. button-link:: https://docs.google.com/presentation/d/1Igq1NgMJezGteWF65DhPTGeK4dpJhrciZkZTKMUhlbQ/edit?usp=sharing
    :color: primary
    :shadow:

    Slides

- **JAWS Usage - 08/22/2022**

We will demonstrate how to submit, monitor, and debug workflows on JAWS. 

.. button-link:: https://docs.google.com/presentation/d/1ZvOhuXjkb57LbA9hxRp-2NY5l1JaUVs13BlLLr0VEFA/edit?usp=sharing
    :color: primary
    :shadow:

    Slides

- **Building a Real WDL - 09/12/2022**

The Hackathon will provide the opportunity for users to gain experience writing WDLs by contributing commonly used tools to the JGI WDL library. We will also practice creating a Dockerfile and WDL task from a Python app in git. 


- **Resources Management - Postponed** 

In this session, you will learn how the backend to JAWS (HTCondor) works. Basic understanding of the backend will inform the user on how to structure the runtime section to allocate the proper compute resources. We will also examine how to use the performance plots available in the JAWS Dashboard to optimize WDL performance.

.. button-link:: https://docs.google.com/forms/d/e/1FAIpQLSfL9DvJ29dJzA1_rzA1xZOK5qMcjL86GCsYZXBy7K-XrumAlQ/viewform?usp=sf_link
    :color: primary
    :shadow:

    Register here!

.. dropdown:: Agenda
    :color: info
    :animate: fade-in

    **Intro to WDL - 08/15/202**

    01:30 - 4:00 PM
  
    - Introduction to WDL
    - Developing WDL - Hand-on activity
  
    **Intro to Docker Containers - 08/18/2022**
  
    01:30 - 4:00 PM
  
    - Introduction to Docker Containers
    - Build your Docker Container
  
    **JAWS Usage - 08/22/2022**
  
    01:30 - 4:00 PM
  
    - JAWS Architecture and Commands
    - Run Tutorial from Docker to JAWS Examples 
    - Debugging Common Issues 
  
    **Hackathon - 09/12/2022**
  
    01:30 - 4:00 PM
  
    - Building a Real WDL (TBD)
  
    **Resources Management - Postponed**
  
    01:30 - 4:00 PM
  
    - Learn how the backend (HTCondor) works
    - Allocate compute resources using the runtime section
    - Backend Demo video  
    - Examining the performance plots to optimize performance


JAWS Workshop - 27 and 28 June 2022
===================================

JAWS (JGI Analysis Workflow Service) is hosting our next Introductory Workshop on JAWS implementation and WDL (Workflow Description Language) creation on Jun 27 and 28. 

On day-1, We will provide a working knowledge of how to submit, monitor, and debug workflows on JAWS. We will also cover the basics of how to create your own WDL. 

On day-2, Attendees will run through some of the tutorials to showcase common WDL-development scenarios. 

.. button-link:: https://docs.google.com/forms/d/e/1FAIpQLSdroQvb9_eECI1v1PQUjW2gQWQtgL6D1otUFOxCgOtae6e5eA/viewform?usp=sf_link
    :color: primary
    :shadow:

    Register here!

.. dropdown:: Agenda
    :color: info
    :animate: fade-in

    **Day 1**

    10:00 AM
	
    - JAWS Architecture
    - JAWS Commands

    10:30 AM
	
    - Docker Containers
    - Condor Backend

    11:35 AM
	
    - Intro to WDLs

    12:00 AM 
	
    - Lunch break

    1:00 PM
	
    - Developing WDLs

    3:00 PM
	
    - Best practices
    - How to find help

    **Day 2**

    10:00 AM 
	
    - Run tutorial from Docker to JAWS examples 
    - Debugging common issues 

    12:00 AM 
	
    - Lunch break

    1:00 PM 
	
    - Demo of Graphical WDL generator
	
    The Almaden Genomics Platform was conceived as a way to make the complex task of developing, deploying and 
    maintaining bioinformatics pipelines quicker, easier and more collaborative.

    1:40 PM - 4:00 PM
	
    - Common WDL designs (scattering, reference data, sub-workflows)
    - Open for custom projects





