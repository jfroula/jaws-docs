==================================
Some Potentially Useful Resources
==================================

* `Validating WDLs output <https://code.jgi.doe.gov/official-jgi-workflows/wdl-pytest-tutorial/-/blob/main/README.md>`_: The advantage of using the pytest-workflows framework over just running a WDL with cromwell.jar is that you are able to design tests for the output.


