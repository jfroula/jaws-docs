========================================================
HTCondor Backend Configuration Options When Creating WDL
========================================================

.. role:: bash(code)
   :language: bash

.. role:: red


Use the following tables to help figure out how to configure your :bash:`runtime{}` section.

How to Allocate Resources in your Runtime Section
=================================================

HTCondor is the back end to Cromwell and is responsible for grabbing the appropriatly sized resource from slurm for each wdl-task. 
HTCondor can determine what resource your task needs from only :bash:`memory` and :bash:`cpu` which is set in the :bash:`runtime{}` section. 
In fact, :bash:`memory` and :bash:`cpu` have defaults set to "2G" and 1(threads), respectively, so you don't have to include them but it is advised for reproducibility.

.. note::
    Inside your :bash:`runtime{}` section of the WDL, :bash:`cpu` key should be set to :bash:`threads` and not cpus, despite the name, because HTCondor expects that value to be :bash:`threads`.


Table of available resources
============================

+---------------------+-----------+--------+-----------+---------+---------+
| Site                |  Type     | #Nodes | Mem (GB)* | Minutes | #Threads|
+=====================+===========+========+===========+=========+=========+
| Perlmutter (NERSC)  | Large     |  3072  |  492      |   2865  |  256    |
+---------------------+-----------+--------+-----------+---------+---------+
| JGI                 | Small     |   40   |  58       |   4305  |   16    |
+ (Lab-IT)            +-----------+--------+-----------+---------+---------+
|                     | Large     |   6    | 492       |   4305  |   32    |
+---------------------+-----------+--------+-----------+---------+---------+
| Dori                | Large     |   100  |  492      |   4305  |   64    |
+ (Lab-IT)            +-----------+--------+-----------+---------+---------+
|                     | Xlarge    |   18   | 1480      |   4305  |   72    |
+---------------------+-----------+--------+-----------+---------+---------+
| Tahoma              | Medium    |   184  |  364      |   2865  |   36    |
+ (EMSL)              +-----------+--------+-----------+---------+---------+
|                     | Xlarge    |   24   | 1480      |   2865  |   36    |
+---------------------+-----------+--------+-----------+---------+---------+
| Defiant (OLCF)      | Medium    |   36   |  256      |   1425  |   128   |
+---------------------+-----------+--------+-----------+---------+---------+

Note: `Defiant` site is not available yet.

.. admonition:: Memory Overhead

    This number is the gigabytes you can actually use because of overhead. 
    For example, on dori, a "large" node is advertized at 512G but since there is overhead, we will reserve 20G and instead ask for 492G in our WDL.

.. admonition:: Time Overhead

    When Cromwell submits a task, HTCondor manages job scheduling by checking the queue for available resources. The JAWS Pool Manager monitors HTCondor and, when needed, requests new Slurm nodes. Once a compute node is available, HTCondor submits the task.
    
    Due to a slight delay (a few seconds) in resource allocation, we build in a time buffer to ensure jobs get the full requested time. For example, instead of requesting the maximum 48 hours on Perlmutter, we request 47 hours and 45 minutes to account for the delay.

Links to documentation about each cluster:
------------------------------------------
* `Dori cluster <https://docs.jgi.doe.gov/display/DORI/Dori>`_    
* `Perlmutter cluster <https://docs.nersc.gov/systems/perlmutter/architecture/>`_
* `Lawrencium cluster <https://it.lbl.gov/service/scienceit/high-performance-computing/lrc/computing-on-lawrencium/>`_  
* `Tahoma cluster <https://www.emsl.pnnl.gov/MSC/UserGuide/tahoma/tahoma_overview.html>`_  
* `Defiant Cluster <https://docs.olcf.ornl.gov/ace_testbed/defiant_quick_start_guide.html>`_  

****************
Runtime Examples
****************

.. note:: Remember that in your :bash:`runtime{}` section, the number you give :bash:`cpu:` is interpreted by HTCondor to be threads not cpu.

.. dropdown:: What would the :bash:`runtime{}` section look like if your task required 8 threads and "5G" RAM.
    :color: info
    :animate: fade-in

    .. code-block:: text
    
        runtime {
            memory: "5G"
            cpu: 8
        }

    For a `dori` node (492G usable ram, threads: 64), you could run 8 tasks in parallel because (64 threads/8 = 8) and you would have more than the required 5G of ram per task since (492G/8 = 61.5G).


.. dropdown:: What happens if I request 64 threads but only 2G of the possible 128G of ram?
    :color: info
    :animate: fade-in

    A regular `dori` node has 492G and 64 threads. So what happens if you request 

    .. code-block:: text
    
        runtime {
            memory: "2G"
            cpu: 64
        }

    Will you be restricted to 2G or will you have access to 492G? 
    Since we don't have any memory limits in place in HTCondor you would be allowed to use all 492G on the node. 
    The reverse should be true as well, if you ask for 492G but only 2 threads (cpu: 2), you should still have access to 64 threads.

.. dropdown:: How should I set the :bash:`runtime{}` section when I want to run many scattering jobs and use multiple nodes.
    :color: info
    :animate: fade-in

    For example, if you request ~500 tasks each with :bash:`runtime{}`:

    .. code-block:: text
    
        runtime {
            memory: "8G"
            cpu: 4
        }

    HTCondor will put them in the queue and the pool manager will start getting new nodes. 
    If we had access to a maximum of 30 nodes, it would grab 30 nodes and start running as many parts of the scatter it can. 
    If nothing else is running you could get 480 of them to run at the same time.
