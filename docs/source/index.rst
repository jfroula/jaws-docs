======================
Documentation for JAWS
======================

.. role:: chocolate

.. figure:: /Figures/JAWS_2.png
    :scale: 100%

Welcome to the official documentation for JAWS, the JGI Analysis Workflow Service, developed by the `Joint Genome Institute <https://jgi.doe.gov>`_ (JGI). 
JAWS is designed to streamline the process of running computational workflows across high-performance computing (HPC), cluster, and cloud environments, improving workflow reusability and efficiency.
 

Summary of JAWS
---------------

* JAWS should simplify your workflow submissions because it takes care of resource configuration and scheduling.
* Before using JAWS, workflows need to be written in the human readable `Workflow Description Language (WDL) <https://software.broadinstitute.org/wdl/>`_.
* JAWS commands can be integrated into larger workflows since everything is `CLI <https://en.wikipedia.org/wiki/Command-line_interface>`_ based.


Getting Started with JAWS
-------------------------

Ready to start? Whether you're writing your own workflows or running existing ones, here’s how to dive in:

* `Build Your Own WDLs <Tutorials/building_wdls.html>`_: Learn how to create workflows using WDL, with tips and best practices for building robust and scalable pipelines.
* `Configure the JAWS Client <jaws/jaws_cli.html>`_: Follow step-by-step instructions to set up and configure the JAWS Client on your system.
* `Run an existing WDL in JAWS <jaws/jaws_quickstart.html>`_: Start executing workflows with JAWS by following this quickstart guide.


.. toctree::
   :hidden:
   :name: extradoc
   :maxdepth: 1
   :caption: Intro

   Why use JAWS <Intro/why_jaws>
   What is JAWS <Intro/how_jaws>
   JAWS Video Library<Intro/videos>
   Getting Help <Intro/contact_us>
   FAQ <Intro/faq>

.. toctree::
   :hidden:
   :name: getstarted
   :maxdepth: 2
   :caption: JAWS

   Configuring the JAWS Client <jaws/jaws_cli>
   Quickstart Example <jaws/jaws_quickstart>
   JAWS Commands <jaws/jaws_usage>
   JAWS API <jaws/jaws_api.rst>
   JAWS Teams <jaws/jaws_teams>
   JAWS Notifications <jaws/jaws_notification>
   JAWS Fair-share Policies <jaws/jaws_policies.rst>
   JAWS Guidelines <jaws/jaws_guide.rst>
   JAWS Backend <jaws/jaws_backend.rst>
   JAWS Refdata <jaws/jaws_refdata.rst>
   JAWS Troubleshooting <jaws/jaws_troubleshooting>

.. toctree::
   :hidden:
   :name: specsdoc
   :maxdepth: 1
   :caption: General Resources

   Compute Resources <Resources/compute_resources.rst>
   Cromwell <Specifications/cromwell>
   Best Practices for WDLs <Resources/best_practices>
   Working Examples <Tutorials/example_wdls>
   JGI Subworkflows and Tasks <Resources/subworkflows>
   Installing MySQL for Cromwell <Specifications/mysql_for_cromwell.rst>

.. toctree::
   :hidden:
   :name: wdldevelop
   :maxdepth: 3
   :caption: Tutorials
   
   Lesson 1: Development Environment <Tutorials/wdl_development>
   Lesson 2: Create Docker Container <Tutorials/create_env>
   Lesson 3: Write a WDL <Tutorials/building_wdls>
   Lesson 4: Defining the Input Data <Tutorials/inputs_json>

.. toctree::
   :hidden:
   :name: release
   :maxdepth: 3
   :caption: JAWS Release 

   JAWS Release Notes <Release/release_notes>

.. toctree::
   :hidden:
   :name: training
   :maxdepth: 2
   :caption: JAWS Events

   Upcoming JAWS Events <training/upcoming-workshop>
   JAWS Events Archive <training/archive>

.. toctree::
   :hidden:
   :name: extras
   :maxdepth: 3
   :caption: Extra Links

   Tutorial for Testing WDLs with Pytest <Tutorials/extras>
