# Changelog

Versions of this documentation release should match the version of JAWS, at least for the first two version numbers (i.e. 2.1). Version numbers of mini-updates (i.e. the 0) may differ. This may be confusing to some, but it helps me keep the versions synced up but allowing for some flexibility.

### v2.1.0

* The description the the JAWS output has been updated from v2.0.

